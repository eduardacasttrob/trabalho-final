<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;

class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }
   public function actionRelatorio1()
   {
       $consulta = new SqlDataProvider([
        'sql' => ' SELECT count(nome) from ae_pessoa',
        
            ]
        );
        return $this->render('relatorio1', ['resultado' => $consulta]);
   }

   public function actionRelatorio2()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT count(*) from ae_categoria',
            ]
        );
        
        return $this->render('relatorio2', ['resultado' => $consulta]);
   }
 
   public function actionRelatorio3()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT count(*) from ae_tem',
            ]
        );
        
        return $this->render('relatorio3', ['resultado' => $consulta]);
   }

   
   public function actionRelatorio4()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT sum(PRECO) from ae_tem',
            ]
 
        );
        return $this->render('relatorio4', ['resultado' => $consulta]);
   }



   public function actionRelatorio5()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT (EMAIL) from ae_pessoa',
            ]
 
        );
        return $this->render('relatorio5', ['resultado' => $consulta]);
   }




}

