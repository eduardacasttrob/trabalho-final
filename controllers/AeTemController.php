<?php

namespace app\controllers;

use Yii;
use app\models\AeTem;
use app\models\AeTemSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AeTemController implements the CRUD actions for AeTem model.
 */
class AeTemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AeTem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AeTemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AeTem model.
     * @param integer $PRODUTO
     * @param integer $PESSOA
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($PRODUTO, $PESSOA)
    {
        return $this->render('view', [
            'model' => $this->findModel($PRODUTO, $PESSOA),
        ]);
    }

    /**
     * Creates a new AeTem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AeTem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PRODUTO' => $model->PRODUTO, 'PESSOA' => $model->PESSOA]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AeTem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $PRODUTO
     * @param integer $PESSOA
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($PRODUTO, $PESSOA)
    {
        $model = $this->findModel($PRODUTO, $PESSOA);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'PRODUTO' => $model->PRODUTO, 'PESSOA' => $model->PESSOA]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AeTem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $PRODUTO
     * @param integer $PESSOA
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($PRODUTO, $PESSOA)
    {
        $this->findModel($PRODUTO, $PESSOA)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AeTem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $PRODUTO
     * @param integer $PESSOA
     * @return AeTem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($PRODUTO, $PESSOA)
    {
        if (($model = AeTem::findOne(['PRODUTO' => $PRODUTO, 'PESSOA' => $PESSOA])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
