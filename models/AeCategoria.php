<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ae_categoria".
 *
 * @property string $NOME
 * @property int $ID
 *
 * @property AeProduto[] $aeProdutos
 */
class AeCategoria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ae_categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NOME'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NOME' => 'Nome',
            'ID' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAeProdutos()
    {
        return $this->hasMany(AeProduto::className(), ['CATEGORIA' => 'ID']);
    }
}
