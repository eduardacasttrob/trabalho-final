<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ae_produto".
 *
 * @property string $NOME
 * @property int $CATEGORIA
 * @property int $ID
 *
 * @property AeCategoria $cATEGORIA
 * @property AeTem[] $aeTems
 * @property AePessoa[] $pESSOAs
 */
class AeProduto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ae_produto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CATEGORIA'], 'integer'],
            [['NOME'], 'string', 'max' => 50],
            [['CATEGORIA'], 'exist', 'skipOnError' => true, 'targetClass' => AeCategoria::className(), 'targetAttribute' => ['CATEGORIA' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NOME' => 'Nome',
            'CATEGORIA' => 'Categoria',
            'ID' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCATEGORIA()
    {
        return $this->hasOne(AeCategoria::className(), ['ID' => 'CATEGORIA']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAeTems()
    {
        return $this->hasMany(AeTem::className(), ['PRODUTO' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPESSOAs()
    {
        return $this->hasMany(AePessoa::className(), ['ID' => 'PESSOA'])->viaTable('ae_tem', ['PRODUTO' => 'ID']);
    }
}
