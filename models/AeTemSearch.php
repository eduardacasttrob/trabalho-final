<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AeTem;

/**
 * AeTemSearch represents the model behind the search form of `app\models\AeTem`.
 */
class AeTemSearch extends AeTem
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PRODUTO', 'SITUACAO', 'PESSOA'], 'integer'],
            [['QUANTIDADE', 'PRECO'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AeTem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'PRODUTO' => $this->PRODUTO,
            'QUANTIDADE' => $this->QUANTIDADE,
            'SITUACAO' => $this->SITUACAO,
            'PRECO' => $this->PRECO,
            'PESSOA' => $this->PESSOA,
        ]);

        return $dataProvider;
    }
}
