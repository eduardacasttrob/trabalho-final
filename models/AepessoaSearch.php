<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AePessoa;

/**
 * AepessoaSearch represents the model behind the search form of `app\models\Aepessoa`.
 */
class AepessoaSearch extends AePessoa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DATA', 'NOME', 'EMAIL', 'LOGIN', 'SENHA'], 'safe'],
            [['ID'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AePessoa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'DATA' => $this->DATA,
            'ID' => $this->ID,
        ]);

        $query->andFilterWhere(['like', 'NOME', $this->NOME])
            ->andFilterWhere(['like', 'EMAIL', $this->EMAIL])
            ->andFilterWhere(['like', 'LOGIN', $this->LOGIN])
            ->andFilterWhere(['like', 'SENHA', $this->SENHA]);

        return $dataProvider;
    }
}
