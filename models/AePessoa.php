<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ae_pessoa".
 *
 * @property string $DATA
 * @property string $NOME
 * @property string $EMAIL
 * @property string $LOGIN
 * @property string $SENHA
 * @property int $ID
 *
 * @property AeTem[] $aeTems
 * @property AeProduto[] $pRODUTOs
 */
class AePessoa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ae_pessoa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DATA'], 'safe'],
            [['NOME', 'EMAIL', 'LOGIN'], 'string', 'max' => 50],
            [['SENHA'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DATA' => 'Data',
            'NOME' => 'Nome',
            'EMAIL' => 'Email',
            'LOGIN' => 'Login',
            'SENHA' => 'Senha',
            'ID' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAeTems()
    {
        return $this->hasMany(AeTem::className(), ['PESSOA' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUTOs()
    {
        return $this->hasMany(AeProduto::className(), ['ID' => 'PRODUTO'])->viaTable('ae_tem', ['PESSOA' => 'ID']);
    }
}
