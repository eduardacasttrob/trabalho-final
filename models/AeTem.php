<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ae_tem".
 *
 * @property int $PRODUTO
 * @property string $QUANTIDADE
 * @property int $SITUACAO
 * @property string $PRECO
 * @property int $PESSOA
 *
 * @property AePessoa $pESSOA
 * @property AeProduto $pRODUTO
 */
class AeTem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ae_tem';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PRODUTO', 'PESSOA'], 'required'],
            [['PRODUTO', 'SITUACAO', 'PESSOA'], 'integer'],
            [['QUANTIDADE', 'PRECO'], 'number'],
            [['PRODUTO', 'PESSOA'], 'unique', 'targetAttribute' => ['PRODUTO', 'PESSOA']],
            [['PESSOA'], 'exist', 'skipOnError' => true, 'targetClass' => AePessoa::className(), 'targetAttribute' => ['PESSOA' => 'ID']],
            [['PRODUTO'], 'exist', 'skipOnError' => true, 'targetClass' => AeProduto::className(), 'targetAttribute' => ['PRODUTO' => 'ID']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PRODUTO' => 'Produto',
            'QUANTIDADE' => 'Quantidade',
            'SITUACAO' => 'Situacao',
            'PRECO' => 'Preco',
            'PESSOA' => 'Pessoa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPESSOA()
    {
        return $this->hasOne(AePessoa::className(), ['ID' => 'PESSOA']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPRODUTO()
    {
        return $this->hasOne(AeProduto::className(), ['ID' => 'PRODUTO']);
    }
}
