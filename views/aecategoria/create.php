<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Aecategoria */

$this->title = 'Criar nova categoria';
$this->params['breadcrumbs'][] = ['label' => 'categorias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aecategoria-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
