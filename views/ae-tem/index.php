<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AeTemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Disponível';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ae-tem-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Adicionar item', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'PRODUTO',
            'QUANTIDADE',
            'SITUACAO',
            'PRECO',
            'PESSOA',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
