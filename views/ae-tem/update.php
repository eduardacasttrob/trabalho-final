<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AeTem */

$this->title = 'Atualizar disponíveis: ' . $model->PRODUTO;
$this->params['breadcrumbs'][] = ['label' => 'Disponíveis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PRODUTO, 'url' => ['view', 'PRODUTO' => $model->PRODUTO, 'PESSOA' => $model->PESSOA]];
$this->params['breadcrumbs'][] = 'Atualizar';
?>
<div class="ae-tem-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
