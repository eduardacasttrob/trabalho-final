<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AeTem */

$this->title = 'Criar Disponiveis';
$this->params['breadcrumbs'][] = ['label' => 'Disponiveis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ae-tem-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
