<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AeTem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ae-tem-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'PRODUTO')->textInput() ?>

    <?= $form->field($model, 'QUANTIDADE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SITUACAO')->textInput() ?>

    <?= $form->field($model, 'PRECO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PESSOA')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

