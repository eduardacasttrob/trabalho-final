<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AeTemSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ae-tem-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'PRODUTO') ?>

    <?= $form->field($model, 'QUANTIDADE') ?>

    <?= $form->field($model, 'SITUACAO') ?>

    <?= $form->field($model, 'PRECO') ?>

    <?= $form->field($model, 'PESSOA') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
