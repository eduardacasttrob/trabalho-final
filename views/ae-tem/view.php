<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AeTem */

$this->title = $model->PRODUTO;
$this->params['breadcrumbs'][] = ['label' => 'Disponiveis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="ae-tem-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Atualizar', ['update', 'PRODUTO' => $model->PRODUTO, 'PESSOA' => $model->PESSOA], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'PRODUTO' => $model->PRODUTO, 'PESSOA' => $model->PESSOA], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'PRODUTO',
            'QUANTIDADE',
            'SITUACAO',
            'PRECO',
            'PESSOA',
        ],
    ]) ?>

</div>
