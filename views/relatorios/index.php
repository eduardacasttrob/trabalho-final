<?php
 
use yii\helpers\Html;
 
$this->title = 'Relatórios';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<div class="relatorios-index">
 
   <h1><?= Html::encode($this->title) ?></h1>
   <?= Html::a('Número de logins', ['relatorio1'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Quantidade de Categoria', ['relatorio2'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Número de dísponiveis', ['relatorio3'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Valor gasto', ['relatorio4'], ['class' => 'btn btn-success']) ?>
   <?= Html::a('Contato pessoais', ['relatorio5'], ['class' => 'btn btn-success']) ?>


</div>
