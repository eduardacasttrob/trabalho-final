<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Aepessoa */

$this->title = 'Adicionar nova pessoa';
$this->params['breadcrumbs'][] = ['label' => 'pessoas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aepessoa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
