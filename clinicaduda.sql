create database clinicaduda;
use clinicaduda;
create table paciente(
id int auto_increment,
nome varchar(200) not null,
endereco varchar(200),
datanasc date,
primary key(id));
create table medico(
id int auto_increment,
nome varchar(200) not null,
endereco varchar(200),
telefone varchar(20) not null,
especialidade varchar(50),
primary key(id));
create table consulta(
id int auto_increment,
data date,
horario time,
paciente_id int,
medico_id int,
primary key(id),
foreign key(paciente_id) references paciente(id),
foreign key(medico_id) references medico(id));